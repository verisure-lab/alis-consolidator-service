<?php

namespace VerisureLab\Library\ALISConsolidatorService\Payload;

final class Consolidation implements \JsonSerializable
{
    /**
     * @var string
     */
    private $leadId;

    /**
     * @var bool
     */
    private $isConsolidated;

    /**
     * @var array
     */
    private $processes;

    /**
     * @var null|\DateTimeImmutable
     */
    private $consolidatedAt;

    public function __construct(string $leadId, bool $isConsolidated, array $processes, ?\DateTimeImmutable $consolidatedAt = null)
    {
        $this->leadId = $leadId;
        $this->isConsolidated = $isConsolidated;
        $this->processes = $processes;
        $this->consolidatedAt = $consolidatedAt;
    }

    /**
     * Factory from serialized data
     *
     * @param array $data
     *
     * @return Consolidation
     */
    public static function fromSerializedData(array $data): self
    {
        return new static(
            $data['leadId'],
            $data['isConsolidated'],
            $data['processes'],
            $data['consolidatedAt'] ? \DateTimeImmutable::createFromFormat(\DateTime::RFC3339, $data['consolidatedAt']) : null
        );
    }

    /**
     * Get leadId
     *
     * @return string
     */
    public function getLeadId(): string
    {
        return $this->leadId;
    }

    /**
     * Get isConsolidated
     *
     * @return bool
     */
    public function isConsolidated(): bool
    {
        return $this->isConsolidated;
    }

    /**
     * Get processes
     *
     * @return array
     */
    public function getProcesses(): array
    {
        return $this->processes;
    }

    /**
     * Get consolidatedAt
     *
     * @return \DateTimeImmutable|null
     */
    public function getConsolidatedAt(): ?\DateTimeImmutable
    {
        return $this->consolidatedAt;
    }

    public function jsonSerialize(): array
    {
        return [
            'leadId' => $this->leadId,
            'isConsolidated' => $this->isConsolidated,
            'processes' => $this->processes,
            'consolidatedAt' => $this->consolidatedAt ? $this->consolidatedAt->format(\DateTime::RFC3339) : null,
        ];
    }
}