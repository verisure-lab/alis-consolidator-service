<?php

namespace VerisureLab\Library\ALISConsolidatorService\Exception;

class CommandNotFoundException extends \Exception
{
}