<?php

namespace VerisureLab\Library\ALISConsolidatorService\Service;

use Enqueue\Client\Config;
use Interop\Queue\Context;
use VerisureLab\Library\ALISConsolidatorService\Commands;
use VerisureLab\Library\ALISConsolidatorService\Payload\Consolidation;

class LeadConsolidator
{
    /**
     * @var Context
     */
    private $context;

    /**
     * @var string
     */
    private $queueName;

    public function __construct(Context $context, string $queueName)
    {
        $this->context = $context;
        $this->queueName = $queueName;
    }
    
    public function consolidate(Consolidation $payload): void
    {
        $fooQueue = $this->context->createQueue($this->queueName);

        $message = $this->context->createMessage(json_encode($payload));
        $message->setProperty(Config::COMMAND, Commands::CONSOLIDATION_RESPONSE);
        $message->setProperty(Config::CONTENT_TYPE, 'application/json');
        $message->setProperty(Config::PROCESSOR, Commands::resolveProcessor(Commands::CONSOLIDATION_RESPONSE));

        $this->context->createProducer()->send($fooQueue, $message);
    }
}