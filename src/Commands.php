<?php

namespace VerisureLab\Library\ALISConsolidatorService;

use VerisureLab\Library\ALISConsolidatorService\Exception\CommandNotFoundException;

class Commands
{
    public const CONSOLIDATION_RESPONSE = 'consolidation-response';

    private static $processorMap = [
        self::CONSOLIDATION_RESPONSE => 'App\Enqueue\Processor\ConsolidationResponseProcessor',
    ];

    public static function resolveProcessor(string $command): string
    {
        if (!array_key_exists($command, self::$processorMap)) {
            throw new CommandNotFoundException(
                sprintf(
                    'Command "%s" was not found. Available commands are: %s',
                    $command,
                    implode('|', array_keys(self::$processorMap))
                )
            );
        }

        return self::$processorMap[$command];
    }
}