<?php

namespace VerisureLab\Library\ALISConsolidatorService;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use VerisureLab\Library\ALISConsolidatorService\DependencyInjection\ALISConsolidatorServiceExtension;

class ALISConsolidatorServiceBundle extends Bundle
{
    public function getContainerExtension(): ALISConsolidatorServiceExtension
    {
        return new ALISConsolidatorServiceExtension();
    }
}